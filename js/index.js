window.onload = ()=>{
    let gisCenter = [118.552028, 37.560572];
function inittianditu() {
    //天地图
    mapboxgl.accessToken = 'pk.eyJ1Ijoid2FuZ3Rvbmd4dWUiLCJhIjoiY2pzY3E2M2k0MDk3NzN5dDA0Nmtia2h0cCJ9.oP9fEJxOgVzm0dWGvL6tGg';
  
    let cvaUrl =
      'http://t0.tianditu.gov.cn/cia_w/wmts?SERVICE=WMTS&REQUEST=GetTile&VERSION=1.0.0&LAYER=cia&STYLE=default&TILEMATRIXSET=w&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}&FORMAT=tiles&tk=d14f100c37012eb8391569c0977aecca';
    let vecUrl =
      'http://t0.tianditu.gov.cn/img_w/wmts?SERVICE=WMTS&REQUEST=GetTile&VERSION=1.0.0&LAYER=img&STYLE=default&TILEMATRIXSET=w&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}&FORMAT=tiles&tk=d14f100c37012eb8391569c0977aecca';
    let cvaUrl1 =
      'http://t0.tianditu.gov.cn/cva_w/wmts?SERVICE=WMTS&REQUEST=GetTile&VERSION=1.0.0&LAYER=cva&STYLE=default&TILEMATRIXSET=w&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}&FORMAT=tiles&tk=d14f100c37012eb8391569c0977aecca';
    let vecUrl1 =
      'http://t0.tianditu.gov.cn/vec_w/wmts?SERVICE=WMTS&REQUEST=GetTile&VERSION=1.0.0&LAYER=vec&STYLE=default&TILEMATRIXSET=w&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}&FORMAT=tiles&tk=d14f100c37012eb8391569c0977aecca';

    let mapStyle = {
      version: 8,
      glyphs: '/glyphs/mapbox/{fontstack}/{range}.pbf',
      sources: {
        //天地图-卫星图
        td_satellite_CVA: {
          type: 'raster',
          tiles: [`${vecUrl}`],
          tileSize: 256,
        },
        //天地图-卫星图标注
        td_satellite_Vec: {
          type: 'raster',
          tiles: [`${cvaUrl}`],
          tileSize: 256,
        },
        //天地图-矢量图
        td_vetor_CVA: {
          type: 'raster',
          tiles: [`${vecUrl1}`],
          tileSize: 256,
        },
        //天地图-矢量图标注
        td_vetor_Vec: {
          type: 'raster',
          tiles: [`${cvaUrl1}`],
          tileSize: 256,
        },
      },
      layers: [
        {
          id: 'td_satellite_CVA',
          type: 'raster',
          source: 'td_satellite_CVA',
        },
        {
          id: 'td_satellite_Vec',
          type: 'raster',
          source: 'td_satellite_Vec',
        },
        {
          id: 'td_vetor_CVA',
          type: 'raster',
          source: 'td_vetor_CVA',
          layout: {
            visibility: 'none',
          },
        },
        {
          id: 'td_vetor_Vec',
          type: 'raster',
          source: 'td_vetor_Vec',
          layout: {
            visibility: 'none',
          },
        },
      ],
    };
    return mapStyle;
  }
const initMap = async () => {
    //mapbox
    let mapStyle = inittianditu()
    map = new mapboxgl.Map({
      container: 'd-mapbox',
      style: mapStyle,
      zoom: 9,
      center: gisCenter,
      crs: 'EPSG:4326',
      antialias: true,
      //maxBounds: bounds,
    });
    //地图加载完毕
    map.on('load', async () => {
      console.log('中心点', map.getCenter());
  
      //设置天空盒子
      map.addLayer({
        id: 'sky',
        type: 'sky',
        paint: {
          'sky-type': 'atmosphere',
          'sky-atmosphere-sun': [0.0, 0.0],
          'sky-atmosphere-sun-intensity': 15,
        },
      });
  
      //油路管线添加hover事件
      map.on('mouseenter', 'pipelines', function (e) {
        map.getCanvas().style.cursor = 'pointer';
      });
  
      //添加hover事件
      map.on('mouseleave', 'pipelines', function (e) {
        map.getCanvas().style.cursor = '';
      });
  
      //油路管线添加hover事件
      map.on('mouseenter', 'pipelinesMedium', function (e) {
        map.getCanvas().style.cursor = 'pointer';
      });
  
      //添加hover事件
      map.on('mouseleave', 'pipelinesMedium', function (e) {
        map.getCanvas().style.cursor = '';
      });
  
      //气路管线添加hover事件
      map.on('mouseenter', 'pipelinesqi', function (e) {
        map.getCanvas().style.cursor = 'pointer';
      });
      //添加hover事件
      map.on('mouseleave', 'pipelinesqi', function (e) {
        map.getCanvas().style.cursor = '';
      });

      //添加油路管线点击事件
      map.on('click', 'pipelines', function (e) {
        console.log('油路管线点击事件', e);
  
        if (e.features.length > 0) {
          let feature = findLineByFeatures(e.features);
          // 触发选中管道列表中的管道事件
          store.commit('app/setClickPipeIdFromMap', feature.properties.id)
          clickPipeline(feature);
        }
      });
  
      //添加油路管线点击事件
      map.on('click', 'pipelinesMedium', function (e) {
        console.log('油路管线点击事件', e);
  
        if (e.features.length > 0) {
          let feature = findLineByFeatures(e.features);
          // 触发选中管道列表中的管道事件
          store.commit('app/setClickPipeIdFromMap', feature.properties.id)
          clickPipeline(feature);
        }
      });
      //地图点击事件
      map.on('click', e => {
        const { lng, lat } = e.lngLat;
        let features = map.queryRenderedFeatures(e.point);
        console.log('map click:', `${lng},${lat}`);
        console.log('map click:' + features);
      });
  
      //缩放结束
      map.on('moveend', () => {
        //改变大小
        setSize();
        // 绘制3D 管道
        //效果不好 先不要了
        // draw3DPioelines();
      });
  
      initState = true;
    }); //loaded map
  };
  initMap()
}